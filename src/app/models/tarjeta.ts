export class TarjetaCredito{
  id?: string;
  titular: string;
  numeroTarjeta: string;
  cvv: number;
  fechaCreacion: Date;
  FechaActualizada: Date;


  constructor(titular: string, numeroTarjeta: string, cvv:number){
    this.titular = titular;
    this.numeroTarjeta = numeroTarjeta;
    this.cvv = cvv;
    this.fechaCreacion = new Date()
    this.FechaActualizada = new Date()
  }
}